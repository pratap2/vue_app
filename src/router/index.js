import Vue from 'vue'
import Router from 'vue-router'
import auth from '@/components/auth/auth'
import myfeed from '@/pages/myfeed/myfeed'
import crypto from '@/pages/crypto/crypto-page'
import details from '@/pages/details/details'
import UserProfile from '@/pages/user-profile/user-profile'
import UserSetting from '@/pages/user-setting/user-setting'
import news from '@/pages/news/news'
import ICOList from '@/pages/ico/ico-list'
import ServicesList from '@/pages/services/services-list'
import newsDetail from '@/pages/news/newsDetail'
import Analysts from '@/pages/analysts/analysts'
import events from '@/pages/events/event'
import eventDetail from '@/pages/events/eventdetail'
import Insights from '@/pages/insights/insights'
import Reviews from '@/pages/reviews/reviews'
import AddReviews from '@/pages/review-add/review-add'
import AddSuggestedEvent from '@/pages/suggested-event/add-suggested-event'
import SuggestedEventList from '@/pages/suggested-event/suggested-event-list'
import AddPredictions from '@/pages/prediction-add/prediction-add'
import Predictions from '@/pages/predictions/predictions'
import Peoplenew from '@/pages/people/people-new'
import Portfolio from '@/pages/user-portfolio/user-portfolio'
import AddInsight from '@/pages/insight-add/insight-add'
import People from '@/pages/people/people'
import peopledetail from '@/pages/people/people-detail'
import appConfig from '../api-service/appconfig'
//import reviewchart from '@/components/review-chart/review-chart'


const axios = require('axios');

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login-init',
      component: auth,
      props: { pageType: 'login' }
    },
    {
      path: '/login',
      name: 'login',
      component: auth,
      props: { pageType: 'login' }
    },
    {
      path: '/signup',
      name: 'signup',
      component: auth,
      props: { pageType: 'signup' }
    },
    {
      path: '/myfeed',
      name: 'myfeed_page',
      component: myfeed
    },  
    {
      path: '/crypto',
      name: 'crypto_page',
      component: crypto
    },
    {
      path: '/details/:name/:id',
      name: 'details',
      component: details
    },
    {
      path: '/user-profile',
      name: 'user-profile',
      component: UserProfile
    },
    {
      path: '/user-setting',
      name: 'user-setting',
      component: UserSetting
    },    
    {
      path: '/news',
      name: 'news',
      component: news
    },
    {
      path: '/newsDetail',
      name: 'newsDetail',
      component: newsDetail
    },
    {
      path: '/analysts',
      name: 'analysts',
      component: Analysts
    },    
    {
      path: '/event',
      name: 'event',
      component: events
    },
    {
      path: '/eventdetail',
      name: 'eventDetail',
      component: eventDetail
    },
    {
      path: '/insights',
      name: 'insights',
      component: Insights
    },
    {
      path: '/reviews/',
      name: 'reviews',
      component: Reviews
    },
    {
      path: '/add-review/:cryptoId',
      name: 'add-review',
      component: AddReviews
    },
    {
      path: '/add-suggested-event/:cryptoId',
      name: 'add-suggested-event',
      component: AddSuggestedEvent
    },
    {
      path: '/edit-suggested-event/:eventId',
      name: 'edit-suggested-event',
      component: AddSuggestedEvent
    },
    {
      path: '/suggested-event-list',
      name: 'suggested-event-list',
      component: SuggestedEventList
    },
    {
      path: '/edit-review/:reviewId',
      name: 'edit-review',
      component: AddReviews
    },
    {
      path: '/predictions/',
      name: 'predictions',
      component: Predictions
    },
    {
      path: '/people-new/',
      name: 'people-new',
      component: Peoplenew
    },   
    {
      path: '/add-prediction/:cryptoId',
      name: 'add-prediction',
      component: AddPredictions
    },
    {
      path: '/edit-prediction/:predictionId',
      name: 'edit-prediction',
      component: AddPredictions
    },
    {
      path: '/portfolio',
      name: 'user-portfolio',
      component: Portfolio
    },
    {
      path: '/add-insight/:cryptoId',
      name: 'add-insight',
      component: AddInsight
    },
    {
      path: '/edit-insight/:cryptoId/:insightId',
      name: 'edit-insight',
      component: AddInsight
    },
    {
      path: '/people',
      name: 'people',
      component: People
    },
    {
      path: '/people-detail',
      name: 'people-detail',
      component: peopledetail
    },
    {
      path: '/ico-list',
      name: 'ico-list',
      component: ICOList
    },
    {
      path: '/services-list',
      name: 'services-list',
      component: ServicesList
    }
  ]
})

const openPages = ['login-init', 'login', 'signup']
router.beforeEach((to, from, next) => {
  if(openPages.includes(to.name)){
    if((to.name == "login" || to.name == 'login-init') && localStorage.getItem("utrum-token")){
      next({name: 'crypto_page'})
    }
    else{
      next()
    }
  }
  else{
    if(localStorage.getItem("utrum-token")){
      next()
    }
    else{
      next({name: 'login'})
    }
  }
})

// HTTP request interceptor
axios.interceptors.request.use(function (config) {
  // add custom acces token in HTTP header here for all request
  if(config.url.includes(appConfig.baseUrl)){
    if(localStorage.getItem("utrum-token")){
      config.headers["Authorization"] =`Bearer ${localStorage.getItem("utrum-token")}`;
    }
    config.headers["Accept"] = "application/json";
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});

// HTTP response interceptor
axios.interceptors.response.use(function (response) {
  // if user is not authorized direct him to login page
  console.log("response ",response);
  return response;
}, function (error) {
  // Do something with response error
  if(error.response.status == 401){
    localStorage.removeItem("utrum-token");
    router.push({ name: 'login' });
  }
  return Promise.reject(error);
});

export default router
