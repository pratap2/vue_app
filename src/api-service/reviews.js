const axios = require('axios');
import appConfig from './appconfig'

export default {
    add: function(params){
        // const CancelToken = axios.CancelToken;
        // const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/reviews`;

        // params['cancelToken'] = source.token
        const promise = axios.post(url, params);
        return {
            promise
        }
    },
    update: function(id, params){
        // const CancelToken = axios.CancelToken;
        // const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/reviews/${id}`;

        // params['cancelToken'] = source.token
        const promise = axios.put(url, params);
        return {
            promise
        }
    },
    delete: function(id, params){
        // const CancelToken = axios.CancelToken;
        // const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/reviews/${id}`;

        // params['cancelToken'] = source.token
        const promise = axios.delete(url, params);
        return {
            promise
        }
    },
    getAll: function(params){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/reviews`;
        const promise = axios.get(url, {params: params, cancelToken: source.token});
        return {
            promise, source
        }
    },
    getById: function(id, params){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/reviews/${id}`;

        params['cancelToken'] = source.token
        const promise = axios.get(url, params);
        return {
            promise, source
        }
    }
}  