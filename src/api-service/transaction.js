const axios = require('axios');
import appConfig from './appconfig'

export default {
    add: function(params){
        let url = `${appConfig.baseUrl}/transactions`;
        const promise = axios.post(url, params);
        return {
            promise
        }
    }
}