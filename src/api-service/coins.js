const axios = require('axios');
import appConfig from './appconfig'

export default {
    getCoins: function(params){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/coins`;
        // return axios.get(url, params)
        params['cancelToken'] = source.token
        const promise = axios.get(url, params);
        return {
            promise, source
        }
        //   .then(response => (this.info = response))
    },
    getCoinsDetail: function(id, params){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/coins/${id}`;

        params['cancelToken'] = source.token
        const promise = axios.get(url, params);
        return {
            promise, source
        }
    },
    getCoinsList: function()
    {
        let url = `${appConfig.baseUrl}/coins`;
        const promise = axios.get(url);
        return {
            promise
        }  
    },
    search: function(params){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/coins/search_coin`;
        // return axios.get(url, params)
        // params['cancelToken'] = source.token
        const promise = axios.get(url, {params: params, cancelToken: source.token});
        return {
            promise, source
        }
        //   .then(response => (this.info = response))
    },
    follow: function(coinId)
    {
        let url = `${appConfig.baseUrl}/coins/follow`;
        const promise = axios.get(url, { params: {coin_id: coinId} });
        return {
            promise
        }  
    },
    unfollow: function(coinId)
    {
        let url = `${appConfig.baseUrl}/coins/unfollow`;
        const promise = axios.get(url, { params: {coin_id: coinId} });
        return {
            promise
        }  
    },
    addToPortfolio: function(coinId)
    {
        let url = `${appConfig.baseUrl}/portfolios/add_to_portfolio`;
        const promise = axios.get(url, { params: {coin_id: coinId} });
        return {
            promise
        }  
    },
    removeFromPortfolio: function(coinId)
    {
        let url = `${appConfig.baseUrl}/portfolios/remove_from_portfolio`;
        const promise = axios.get(url, { params: {coin_id: coinId} });
        return {
            promise
        }  
    },
    getPortfolioCoins: function()
    {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/portfolios/list_added_coins`;
        const promise = axios.get(url, {
            cancelToken: source.token, 
            // params: params
        });
        return {
            promise, source
        }
    },
    unfollow: function(coinId)
    {
        let url = `${appConfig.baseUrl}/coins/unfollow`;
        const promise = axios.get(url, { params: {coin_id: coinId} });
        return {
            promise
        }  
    },
    getNews: function(){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/coins/news`;
        const promise = axios.get(url, {params: {}, cancelToken: source.token});
        return {
            promise, source
        }
    },
    getIcos: function(){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/icos`;
        const promise = axios.get(url, {params: {}, cancelToken: source.token});
        return {
            promise, source
        }
    },
    getExchangesData: function(params){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/coins/market_exchange`;
        const promise = axios.get(url, {params: params, cancelToken: source.token});
        return {
            promise, source
        }
    },
    getDataForGraph: function(sdt, edt){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        // let url = `https://graphs2.coinmarketcap.com/currencies/bitcoin/${sdt}/${edt}/`;
        let url = 'https://api.myjson.com/bins/fry0q'
        const promise = axios.get(url, {params: {}, cancelToken: source.token});
        return {
            promise, source
        }
    }
}  