const axios = require('axios');
import appConfig from './appconfig'

export default {
    getRedditCount: function(redditSlug){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `https://www.reddit.com/r/${redditSlug}/about.json`;
        const promise = axios.get(url, {params: {}, cancelToken: source.token});
        return {
            promise, source
        }
    }
}  