const axios = require('axios');
import appConfig from './appconfig'

export default {
    getAnalysts: function(params){
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let url = `${appConfig.baseUrl}/analyst_details`;
        
        params['cancelToken'] = source.token
        const promise = axios.get(url, params);
        return {
            promise, source
        }
        //   .then(response => (this.info = response))
    }
}  