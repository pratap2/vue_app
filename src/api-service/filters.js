import Vue from "vue"
import moment from 'moment'

Vue.filter('date', (value, format) => {
    if (value) {
        try{
            format = format || 'YYYY-MM-DD hh:mm'
            return moment(String(value)).format(format)
        }
        catch(e){
            console.log("Exception :",e);
        }
    }
    else{
        return null
    }
})
Vue.filter("extractTwitterHandle", (str) => {
    console.log("extractTwitterHandle :",str)
    try{
        if(str){
            str = str.trim()
            if(str.indexOf("@") == 0){
                return str
            }
            else{
                let tmepArr = str.split("/")
                return `@${tmepArr[tmepArr.length - 1]}`
            }
        }
        return '-';
    }
    catch(e){
        console.log("extractTwitterHandle Exception: ",e)
        return null
    }
})
Vue.filter("extractRedditHandle", (str) => {
    console.log("extractRedditHandle :",str)
    try{
        if(str){
            str = str.replace(/([\/]*)$/, '')
            let tmepArr = str.split("/")
            return tmepArr[tmepArr.length - 1]
        }
        return null;
    }
    catch(e){
        console.log("extractRedditHandle Exception: ",e)
        return null
    }
})
Vue.filter("buildTwitterUrl", (str) => {
    console.log("buildTwitterUrl :",str)
    try{
        if(str){
            str = str.trim()
            if(str.indexOf("@") == 0){
                return `https://www.twitter.com/${str.split("@")[1]}`
            }
            else{
                return str;
            }
        }
        else{
            return 'javascript: void(0)'
        }
    }
    catch(e){
        console.log("extractTwitterHandle Exception: ",e)
        return 'javascript: void(0)'
    }
})
Vue.filter('roundOff', (value, round) => {
    if(value == "-") return value
    else{
        value = Number(value)
        let returnVal = (parseInt(value * (10 ** (round + 1))) - parseInt(value * (10 ** round)) * 10) > 4 ? (((parseFloat(parseInt((value + parseFloat(1 / (10 ** round))) * (10 ** round))))) / (10 ** round)) : (parseFloat(parseInt(value * (10 ** round))) / ( 10 ** round));
        
        return returnVal
    }
})
Vue.filter('numbersWithComma', (nStr) => {
    if(nStr == "-") return nStr
    else{
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
         x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
})
Vue.filter('numFormatter', (num, digits) => {
    if(num == "-") return num
    if(num == "0") return 'N/A'
    var si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: " K" },
        { value: 1E6, symbol: " M" },
        { value: 1E9, symbol: " B" },
        { value: 1E12, symbol: " T" },
        { value: 1E15, symbol: " P" },
        { value: 1E18, symbol: " E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
            break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
      
})
Vue.filter('percentageChange', (newVal, oldVal) => {
    let decrease = oldVal - newVal
    let per_decrese = ( decrease / oldVal ) * 100
    return per_decrese
})
Vue.filter('formatNumberForLen', (value, len) => {
    if(value == "-") return value
    else{
        value = Number(value).toPrecision(len)
        if(value > 1){
            return Number(value).toFixed(2)
        }
        else{
            return Number(value).toFixed(len > 1 ? (len-1) : len)
        }
    }
})
Vue.filter('truncate', (text, length, clamp) => {
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
})
Vue.filter('showPecentChange', (val) => {
    if(val){
        if(val == 0){
            return val
        }
        else if(Number(val) < 0 ){
            return `${val}%`
        }
        else{
            return `+${val}%`
        }
    }
    else{
        return val
    }
})

Vue.filter('abs', (val) => {
    if(val == "N/A") val=0
    if(val){
        
        return Math.abs(val);
    }
    else{
        return val
    }
})

Vue.filter('currencyConversion', (fromVal, toVal) => {
    // pass fromVal and toVal in USD 
    // for eg. 1 btc = 6424.788 USD and 1 ETH = 215.78
    // so pass fromVal = 6424.788, toVal = 215.78
    // it will return 1 BTC = ? ETH
    let fromUnit =  1 / fromVal
    let toUnit = 1 / toVal

    return toVal / fromVal
})
Vue.filter('convertToInt', (val) => {
    if(val == "-") return val
    else{
        try{
            return parseInt(val)            
        }
        catch(e){
            return 0
        }
    }
})

Vue.filter('formatDate', function(value) {
    if (value) {
      return moment(String(value)).format('D MMMM YYYY')
    }
})

Vue.filter('reverse', function(value) {
    // slice to make a copy of array, then reverse the copy
    return value.slice().reverse();
})

// Vue.filter('sortdec',  (val) => {
//     alert(val);
//     // slice to make a copy of array, then reverse the copy
//     val.sort((a, b) => new Date(a.date) - new Date(b.date))
//     return val.slice().reverse();
// })


