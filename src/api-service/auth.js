const axios = require('axios');
import appConfig from './appconfig'

export default {
    login: function(params){
        let url = `${appConfig.baseUrl}/login`;
        return axios.post(url, params)
        //   .then(response => (this.info = response))
    },
    logout: function(params){
        let url = `${appConfig.baseUrl}/logout`;
        return axios.delete(url, params)
        //   .then(response => (this.info = response))
    },
    signup: function(params){
        let url = `${appConfig.baseUrl}/signup`;
        return axios.post(url, params)
    }
}  