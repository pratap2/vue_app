// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'
import { store } from './store' 
import ToggleButton from 'vue-js-toggle-button'
import './api-service/filters'
import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
import Vue2Crumbs from 'vue-2-crumbs'
import SocialSharing from 'vue-social-sharing'

Vue.use(Vue2Crumbs);
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(ToggleButton);
Vue.use(Datetime);
Vue.use(SocialSharing);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});

// var filter = function(text, length, clamp){
//   clamp = clamp || '...';
//   var node = document.createElement('div');
//   node.innerHTML = text;
//   var content = node.textContent;
//   return content.length > length ? content.slice(0, length) + clamp : content;
// };

// Vue.filter('truncate', filter);
